#!/bin/bash

HOURLY=48
DAILY=31
MONTHLY=12
YEARLY=100

dir="${1}"
snap="${dir}/.snap"

if [ ! -d "${snap}/." ]
then
	"${snap} not available"
	exit 1
	fi

	cd "${snap}"
	last=$(date +%Y-%m-%d-%H)

	[ -d "${last}" ] || mkdir "${last}"

	hour=$(date +%H)


	temp=$(mktemp)

	ls -d ????-01-01-00 2> /dev/null | sort | tail -n ${YEARLY} >> ${temp}
	ls -d ????-??-01-00 2> /dev/null | sort | tail -n ${MONTHLY}  >> ${temp}
	ls -d ????-??-??-00 2> /dev/null | sort | tail -n ${DAILY}  >> ${temp}
	ls -d ????-??-??-?? 2> /dev/null | sort | tail -n ${HOURLY}  >> ${temp}

	ls -d ????-??-??-?? | while read d
do
	grep "${d}" ${temp} > /dev/null
	[ $? == 1 ] && rmdir "${d}"
done

rm ${temp}

cd - > /dev/null
